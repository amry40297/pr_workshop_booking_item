-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2022 at 09:54 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latihanspringamry`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking_transaction`
--

CREATE TABLE `booking_transaction` (
  `id` int(11) NOT NULL,
  `description` varchar(150) NOT NULL,
  `amount` varchar(35) NOT NULL,
  `status` varchar(35) NOT NULL,
  `book_days` int(11) NOT NULL,
  `registered_date` datetime NOT NULL,
  `user_id` int(32) NOT NULL,
  `expired_date` datetime NOT NULL,
  `item_id` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking_transaction`
--

INSERT INTO `booking_transaction` (`id`, `description`, `amount`, `status`, `book_days`, `registered_date`, `user_id`, `expired_date`, `item_id`) VALUES
(12, 'test', '123', 'test', 32, '2022-03-11 01:37:21', 55, '2022-04-12 01:37:21', NULL),
(17, 'test', '123', 'test', 21, '2022-03-09 09:28:35', 12, '2022-03-30 09:28:35', ''),
(1324, 'test', '123', 'test', 32, '2022-03-10 09:22:45', 99, '2022-04-11 09:22:45', ''),
(1324555, 'test', '123', 'test', 32, '2022-03-10 09:33:13', 99, '2022-04-11 09:33:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_item`
--

CREATE TABLE `transaction_item` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `category` varchar(32) NOT NULL,
  `stock` int(11) NOT NULL,
  `status` varchar(1) NOT NULL,
  `cost` int(11) NOT NULL,
  `booking_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transaction_item`
--

INSERT INTO `transaction_item` (`id`, `name`, `category`, `stock`, `status`, `cost`, `booking_id`) VALUES
(1, 'aqua', 'test', 999, '1', 999, 12),
(7, 'aqua', 'test', 999, '1', 999, 12),
(9, 'aqua', 'test', 999, '1', 999, 12),
(88, 'aqua', 'test', 999, '1', 999, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking_transaction`
--
ALTER TABLE `booking_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_item`
--
ALTER TABLE `transaction_item`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking_transaction`
--
ALTER TABLE `booking_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1324556;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
