package com.latihanamry.latihanspringboot.repository;

import com.latihanamry.latihanspringboot.model.Booking;
import com.latihanamry.latihanspringboot.model.Item;
import com.latihanamry.latihanspringboot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


public interface BookingRepository extends JpaRepository<Booking,Long> {
    @Modifying
    @Transactional
    @Query(value="INSERT INTO `booking_transaction` (`id`, `description`, `amount`, `status`, `book_days`, `registered_date`, `user_id`, `item_id`, `expired_date`) " +
            "VALUES (:id, :description, :amount, :status, :book_days, :registered_date, :user_id, :items, :expired_date)", nativeQuery = true)
    Booking saveBooking(@Param("id") String id,
                     @Param("description") String description,
                     @Param("amount") String amount,
                     @Param("status") String status,
                     @Param("book_days") String bookDays,
                     @Param("registered_date") String registeredDate,
                     @Param("user_id") User userId,
                     @Param("expired_date") String expiredDate,
                     @Param("items") List<Item> itemId);
}
