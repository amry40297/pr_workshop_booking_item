package com.latihanamry.latihanspringboot.repository;

import com.latihanamry.latihanspringboot.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<Item,Long> {
}
