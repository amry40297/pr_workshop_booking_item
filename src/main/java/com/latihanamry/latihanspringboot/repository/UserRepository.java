package com.latihanamry.latihanspringboot.repository;

import com.latihanamry.latihanspringboot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
