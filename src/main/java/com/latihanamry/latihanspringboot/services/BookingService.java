package com.latihanamry.latihanspringboot.services;

import com.latihanamry.latihanspringboot.dto.BookingDto;
import com.latihanamry.latihanspringboot.model.Booking;
import com.latihanamry.latihanspringboot.model.Item;
import com.latihanamry.latihanspringboot.model.User;
import com.latihanamry.latihanspringboot.repository.BookingRepository;
import com.latihanamry.latihanspringboot.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Component
public class BookingService {
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private ItemRepository itemRepository;

    public List<BookingDto> getBookingDto(){
        List<Booking> bookings = bookingRepository.findAll();
        ModelMapper mm = new ModelMapper();
        return bookings.stream().map(x -> mm.map(x,BookingDto.class)).collect(Collectors.toList());
    }
    public Booking getOneBookingData(long id) throws Exception{
        try {
            return bookingRepository.getOne(id);
        }
        catch (Exception err)
        {
            return null;
        }
    }

    public HashMap<String,Booking> addBooking(Booking obj) throws Exception{
        HashMap<String,Booking> resultStatus = new HashMap<>();
        try {
            log.info("Adding mybooking");
            obj.setRegisteredDate(getDateTime("YYYY-MM-dd hh:mm:ss"));
            obj.setExpiredDate(getExpiredDateTime("YYYY-MM-dd hh:mm:ss",obj.getBookDays()));
            if(obj.getItemId() != null)
            {
                for (Item item: obj.getItemId()) {
                    item.setBooking(obj);
                    if(!itemRepository.existsById(item.getId())) {
                        resultStatus.put("Data For Item With Id"+item.getId()+"NotFound",null);
                        return resultStatus;
                    }
                    else { itemRepository.save(item); }
                }
            }
            resultStatus.put("Response",bookingRepository.save(obj));
            return resultStatus;
        }
        catch (Exception e){
            log.info("Error Exception : {}",e.getMessage());
            resultStatus.put("Error: saving or updating data",null);
            return resultStatus;
        }
    }
    public static List<Item> itemList(List<String> data) {
        ModelMapper mm = new ModelMapper();
        return data.stream().map(x -> mm.map(x,Item.class)).collect(Collectors.toList());
    }

    private static String getDateTime(String dateTimeFormat) {
        StringBuilder uniqueSequence = new StringBuilder();
        SimpleDateFormat simpleTime = new SimpleDateFormat(dateTimeFormat);
        uniqueSequence.append(simpleTime.format(new Date()));

        return uniqueSequence.toString();
    }
    private static String getExpiredDateTime(String dateTimeFormat,int days) {
        StringBuilder uniqueSequence = new StringBuilder();
        SimpleDateFormat simpleTime = new SimpleDateFormat(dateTimeFormat);
        Date currentDate = new Date();

        // convert date to calendar
        Calendar c = Calendar.getInstance();
        c.setTime(currentDate);

        c.add(Calendar.DATE, days);

        // convert calendar to date
        Date currentDatePlusOne = c.getTime();
        uniqueSequence.append(simpleTime.format(currentDatePlusOne));

        return uniqueSequence.toString();
    }
}
