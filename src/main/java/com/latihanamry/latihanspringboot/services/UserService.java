package com.latihanamry.latihanspringboot.services;

import com.latihanamry.latihanspringboot.dto.UserDto;
import com.latihanamry.latihanspringboot.model.User;
import com.latihanamry.latihanspringboot.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    public List<UserDto> getUserDto(){
        List<User> users = userRepository.findAll();
        ModelMapper mm = new ModelMapper();

        return users.stream().map(x -> mm.map(x, UserDto.class)).collect(Collectors.toList());
    }
}
