package com.latihanamry.latihanspringboot.services;

import com.latihanamry.latihanspringboot.dto.ItemDto;
import com.latihanamry.latihanspringboot.model.Item;
import com.latihanamry.latihanspringboot.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ItemService {
    @Autowired
    private ItemRepository ItemRepository;

    public List<ItemDto> getItemDto(){
        List<Item> Items = ItemRepository.findAll();
        ModelMapper mm = new ModelMapper();
        return Items.stream().map(x -> mm.map(x,ItemDto.class)).collect(Collectors.toList());
    }
    public Item getOneItemData(long id) throws Exception{
        try {
            return ItemRepository.getOne(id);
        }
        catch (Exception err)
        {
            return null;
        }
    }

    public Item addItem(Item obj) throws Exception{
        Map<String,String> StatusCode = new HashMap<>();
        try {
            System.out.println(obj);
            return ItemRepository.save(obj);
        }
        catch (Exception e){
            log.info("Error Exception");
            StatusCode.put("Error","Save / Update Failed");
            return null;
        }


    }
}
