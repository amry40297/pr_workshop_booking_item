package com.latihanamry.latihanspringboot.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
@Slf4j
public class utils {
    public static void generalErrorMessage(Exception e)
    {
        log.info("General Error Internal {} : {}", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
    }
}
