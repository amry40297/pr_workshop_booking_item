package com.latihanamry.latihanspringboot.dto;

import com.latihanamry.latihanspringboot.model.Item;
import com.latihanamry.latihanspringboot.model.User;
import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import java.util.List;

@Data
public class BookingDto {
    private Long id;
    private String description;
    private String amount;
    private String status;
    private int bookDays;
    private String registeredDate;
    private String expiredDate;
    private User userId;

    @ElementCollection
    @Embedded
    private List<Item> itemId;
}