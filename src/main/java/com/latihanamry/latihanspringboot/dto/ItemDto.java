package com.latihanamry.latihanspringboot.dto;

import com.latihanamry.latihanspringboot.model.Booking;
import lombok.Data;


@Data
public class ItemDto {
    private Long id;
    private String name;
    private String category;
    private String stock;
    private String status;
    private int cost;
}
