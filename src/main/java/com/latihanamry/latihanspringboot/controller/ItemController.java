package com.latihanamry.latihanspringboot.controller;

import com.latihanamry.latihanspringboot.dto.ItemDto;
import com.latihanamry.latihanspringboot.model.Item;
import com.latihanamry.latihanspringboot.repository.ItemRepository;
import com.latihanamry.latihanspringboot.services.ItemService;
import com.latihanamry.latihanspringboot.utils.utils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(path = "/item")
public class ItemController {
    @Autowired
    private ItemService itemservice;

    @Autowired
    private ItemRepository itemRepository;

    @PostMapping(value = "/saveItem")
    public ResponseEntity<Item> saveOrUpdateItem(@RequestBody ItemDto Item) throws Exception {
        System.out.println(Item);
        log.info("TEST");
        try {
            ModelMapper mm = new ModelMapper();
            Item data = mm.map(Item,Item.class);
            Item itemData = itemservice.addItem(data);
            return itemData != null ? new ResponseEntity<>(itemData, HttpStatus.CREATED) :  new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            log.info("GET ERROR");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/Detail")
    public ResponseEntity<Item> getBookdetail(@RequestParam Long id) throws Exception {

        try {
            Item bookID = itemservice.getOneItemData(id);
            return bookID != null ? new ResponseEntity<>(bookID,HttpStatus.OK) :  new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @DeleteMapping("/Delete")
    public ResponseEntity<String> deleteBooking(@RequestParam Long id) throws Exception {

        try {
            itemRepository.deleteById(id);
            return !itemRepository.existsById(id) ? new ResponseEntity<>("Delete Succesful",HttpStatus.OK) :  new ResponseEntity<>("Delete Failed No Data Exist", HttpStatus.NOT_FOUND);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>("Delete Failed Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @GetMapping("/listItem")
    public ResponseEntity<List<Item>> getAllBook(Long id) throws Exception {

        try {
            List <Item> listItem = itemRepository.findAll();
            return listItem.get(0) != null ?  ResponseEntity.ok(listItem) :  null;
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
