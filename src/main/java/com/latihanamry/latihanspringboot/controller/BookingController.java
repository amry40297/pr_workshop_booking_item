package com.latihanamry.latihanspringboot.controller;

import com.latihanamry.latihanspringboot.dto.BookingDto;
import com.latihanamry.latihanspringboot.model.Booking;
import com.latihanamry.latihanspringboot.repository.BookingRepository;
import com.latihanamry.latihanspringboot.services.BookingService;
import com.latihanamry.latihanspringboot.utils.utils;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping(path = "/booking")
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BookingRepository  bookingRepository;

    @PostMapping(value = "/saveBooking")
    public ResponseEntity<Booking> saveOrUpdateBooking(@RequestBody BookingDto booking) throws Exception {
        System.out.println(booking);
        log.info("TEST");
        try {
            ModelMapper mm = new ModelMapper();
            Booking data = mm.map(booking,Booking.class);
            HashMap<String,Booking> bookingData = bookingService.addBooking(data);
            Booking response = bookingData.get("Response");
            return response != null ? new ResponseEntity<>(response,HttpStatus.CREATED) :  new ResponseEntity<>(data,HttpStatus.BAD_REQUEST);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            log.info("GET ERROR");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/Detail")
    public ResponseEntity<Booking> getBookdetail(@RequestParam Long id) throws Exception {

        try {
            Booking bookID = bookingService.getOneBookingData(id);
            return bookID != null ? new ResponseEntity<>(bookID,HttpStatus.OK) :  new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @DeleteMapping("/Delete")
    public ResponseEntity<String> deleteBooking(@RequestParam String id) throws Exception {

        try {
            Long reqid = Long.valueOf(id);
            bookingRepository.deleteById(reqid);
            return !bookingRepository.existsById(reqid) ? new ResponseEntity<>("Delete Succesful",HttpStatus.OK) :  new ResponseEntity<>("Delete Failed No Data Exist", HttpStatus.NOT_FOUND);
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>("Delete Failed Internal Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @GetMapping("/listBook")
    public ResponseEntity<List<Booking>> getAllBook(Long id) throws Exception {

        try {
            List <Booking> listBooking = bookingRepository.findAll();
            return listBooking.get(0) != null ?  ResponseEntity.ok(listBooking) :  null;
        }catch (Exception e)
        {
            utils.generalErrorMessage(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
