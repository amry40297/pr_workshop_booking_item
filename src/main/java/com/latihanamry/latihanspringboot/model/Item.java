package com.latihanamry.latihanspringboot.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "transaction_item")
@Embeddable
@NoArgsConstructor
public class Item {
    @Id
    private Long id;
    private String name;
    private String category;
    private String stock;
    private String status;
    private int cost;

    @ManyToOne(fetch = FetchType.LAZY ,cascade = {CascadeType.ALL})
    @JoinColumn(name = "booking_id")
    @JsonBackReference
    private Booking booking;

}
