package com.latihanamry.latihanspringboot.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;


@Data
@Entity
@Table(name = "mst_user")
public class User {
    @Id
    private Long id;
    private String username;
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToMany(mappedBy = "userId",fetch = FetchType.LAZY)
    private List<Booking> bookings;

}