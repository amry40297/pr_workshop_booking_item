package com.latihanamry.latihanspringboot.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "booking_transaction")
public class Booking {
    @Id
    private Long id;
    private String description;
    private String amount;
    private String status;
    private int bookDays;
    @Nullable
    private String expiredDate;
    @Nullable
    private String registeredDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private User userId;

    @OneToMany(mappedBy = "booking",fetch = FetchType.LAZY)
    private List<Item> itemId;

    public List<Item> getItems() {
        return itemId;
    }

    public void setItems(List<Item> item) {
        this.itemId = item;
    }
}
