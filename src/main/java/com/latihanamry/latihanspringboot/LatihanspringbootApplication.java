package com.latihanamry.latihanspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LatihanspringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(LatihanspringbootApplication.class, args);
	}

}
